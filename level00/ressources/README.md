# Level 00

- Here we simply search for a file with the user of `flag00`:
	```bash
	find / -user flag00 2>/dev/null # Redirect errors to null as it's spam
	```

- In `/usr/sbin/john` we get: `cdiiddwpgswtgt`.
- I made this `C` program to '_crack_' the password,
	it's a rotn substitution cipher or cesar cipher:
	```c
	#include <stdio.h>

	int main() {
		char *m = "cdiiddwpgswtgt";

		for (int shift = 0; shift < 26; ++shift) {
			printf("Key[%2i]: ", shift);
			for (int i = 0; m[i]; ++i) {
				printf("%c", ((m[i] - 'a' + shift) % 26) + 'a');
			}
			printf("\n");
		}
	}
	```
- By running the program we get:
	```bash
	~/Projects/snowcrash/ master
	❯ clang cipher.c && ./a.out
	[...]
	Key[11]: nottoohardhere
	[...]
	```
