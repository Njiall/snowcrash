# Level13

- We got a binary in the home directory.
	```bash
	level13@SnowCrash:~$ ./level13
	UID 2013 started us but we we expect 4242
	```

- So here the objective is really clear.

- Here we will be using gdb for the injection of the process id.
	```
	level13@SnowCrash:~$ gdb level13
	[...]
	(gdb) b main
	Breakpoint 1 at 0x804858f
	(gdb) r
	Starting program: /home/user/level13/level13

	Breakpoint 1, 0x0804858f in main ()
	(gdb) disas
	Dump of assembler code for function main:
	   0x0804858c <+0>:	push   %ebp
	   0x0804858d <+1>:	mov    %esp,%ebp
	=> 0x0804858f <+3>:	and    $0xfffffff0,%esp
	   0x08048592 <+6>:	sub    $0x10,%esp
	   0x08048595 <+9>:	call   0x8048380 <getuid@plt>
	   0x0804859a <+14>:	cmp    $0x1092,%eax
	   0x0804859f <+19>:	je     0x80485cb <main+63>
	[...]
	```
- By adding some breakpoint at the comparison we can edit the eax register to
	have the value we want.
	```
	(gdb) b *0x0804859a
	Breakpoint 2 at 0x804859a
	```
- We continue and set the $eax register.
	```
	(gdb) c
	Continuing.

	Breakpoint 2, 0x0804859a in main ()
	(gdb) set $eax=4242
	```
- And then we continue and get the token.
	```
	(gdb) c
	your token is 2A31L79asukciNyi8uppkEuSx
	[Inferior 1 (process 3208) exited with code 050]
	```
