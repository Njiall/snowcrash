# Snow Crash

This a 42 project based around security and vulnerability exploits.

> An introductory project on computer security,
> Snow Crash aims to introduce security in different sub-domains,
> with a developer-oriented approach.
>
> We will familiarize ourselves with several languages
> (ASM / perl / php ..), develop a certain logic to understand unknown programs,
> and thus become aware of the problems related to simple "errors" programming.

## File structure

```bash
- levelXX
	|-> flag # The password of the level
	`-> ressources # Additional informations about the level
		|-> README.md # Explainations on how to get the flag
		`-> ... # Extra scripts and files used to get the flag
```

## Resources

- [Subject](https://cdn.intra.42.fr/pdf/pdf/13254/fr.subject.pdf)

## Explainations

### Mandatory

- [Level00](level00/ressources/README.md)
- [Level01](level01/ressources/README.md)
- [Level02](level02/ressources/README.md)
- [Level03](level03/ressources/README.md)
- [Level04](level04/ressources/README.md)
- [Level05](level05/ressources/README.md)
- [Level06](level06/ressources/README.md)
- [Level07](level07/ressources/README.md)
- [Level08](level08/ressources/README.md)
- [Level09](level09/ressources/README.md)

### Bonus

- [Level10](level10/ressources/README.md)
- [Level11](level11/ressources/README.md)
- [Level12](level12/ressources/README.md)
- [Level13](level13/ressources/README.md)
- [Level14](level14/ressources/README.md)
