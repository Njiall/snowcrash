# Level 02

- Here there is a file direcly in the home directory. `level02.pcap`.
- `pcap` extention is linked to `wireshark` so we will use `wireshark-cli`.
- It seems to be logs of an ssh session. ;)
- At the end of the log we get:
	```bash
	tshark -Tfields -e data -r level02.pcap | tr -d '\n' | xxd -r -p | cat -A
	[...]Password: ft_wandr^?^?^?NDRel^?L0L^M^@^M$[...]
	```
- Meaning we get the password: `ft_waNDReL0L`.
