# Level09

- This one is really easy:
	There is an executable that encrypts a string.
	And hold on, the encryption is adding the index to the char:
	```bash
	level09@SnowCrash:~$ ./level09 abcdef
	acegik
	level09@SnowCrash:~$ ./level09 aaaaaa
	abcdef
	level09@SnowCrash:~$ ./level09 aaaaaaaaaaaaaaaaaaaaaaaaaa
	abcdefghijklmnopqrstuvwxyz
	level09@SnowCrash:~$ ./level09 0000000000
	0123456789
	```
- And we also a token:
	```bash
	level09@SnowCrash:~$ cat token | xxd -p
	66346b6d6d36707c3d827f70826e838244428344757b7f8c890a
	```
- So I wrote a quick and dirty `C` program:
	```c
	#include <stdio.h>

	int hex_to_ascii(char s[2]) {
		int r = 0;
		for (int i = 0; i < 2; ++i) {
			r <<= 4;
			if      (s[i] >= '0' && s[i] <= '9') r |= s[i] - '0';
			else if (s[i] >= 'a' && s[i] <= 'z') r |= s[i] - 'a' + 10;
		}
		return r;
	}

	int main() {
		char *m = "66346b6d6d36707c3d827f70826e838244428344757b7f8c89"; // stripped '\n'
		char s[256] = {};

		for (int i = 0; m[i * 2]; ++i)
			s[i] = hex_to_ascii(m + (i * 2)) - i;
		printf("%s\n", s);
	}
	```
- And using it we get:
	```bash
	~/Projects/snowcrash master
	❯ clang test.c && ./a.out
	f3iji1ju5yuevaus41q1afiuq
	```
