# Level 01

- Here we need to do a similar thing as level00.
- Look at `/etc/passwd` which stores informations on the passwords
of the users. weirdly we have view of the password hash.
	```bash
	level01@SnowCrash:~$ cat /etc/passwd
	[...]
	flag01:42hDRfypTqqnw:3001:3001::/home/flag/flag01:/bin/bash
	[...]
	```
- Use [this](https://www.cyberciti.biz/faq/understanding-etcpasswd-file-format/)
to get what the format means.
- So what we have here is a hash for the password of user flag01.
- We use [john the ripper](https://github.com/openwall/john) to find a possible
password that was used.
	```bash
	~/Projects/snowcrash master
	❯ echo "42hDRfypTqqnw" > passwd && john passwd
	[...]
	abcdefg          (flag01)
	[...]
	```
- EZ GAME
